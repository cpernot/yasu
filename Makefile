CC = gcc
BISON = bison
FLEX = flex
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
CFLAGS = -Os -Wall -Wextra -Wpedantic -Wmissing-prototypes -Wstrict-prototypes -std=gnu99
LFLAGS = -lcrypt -lpam -lpam_misc
SOURCES = yasu.c bison.y parser.l
CSOURCES = bison.c yasu.c parser.c
BIN = yasu
TEMP = bison.c bison.h parser.c
MAN = yasu.1
MANPREFIX = ${PREFIX}/share/man

all: ${BIN}

install: ${BIN}
	mkdir -p -- ${DESTDIR}${BINDIR}
	cp -f -- ${BIN} ${DESTDIR}${BINDIR}
	chown root:root ${DESTDIR}${BINDIR}/${BIN}
	chmod u+s ${DESTDIR}${BINDIR}/${BIN}
	cp ${MAN} ${MANPREFIX}/man1/

yasu: ${CSOURCES} bison.h
	${CC} ${LFLAGS} -o $@ ${CSOURCES}

bison.h bison.c: bison.y
	${BISON} -o bison.c $< --defines=bison.h

parser.c: parser.l
	${FLEX} -o $@ $<

uninstall:
	rm -f ${DESTDIR}${BINDIR}/${BIN}

clean:
	rm -f ${BIN} ${TEMP}
