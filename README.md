
# Yasu
Yet Another Sudo Utility

## Dependencies

* libpam, libpam\_misc
* libpam0g-dev (build time)
* C99 compiler (build time)
* POSIX make (build time)
* POSIX libc & initgroups

## Installation
```sh
make
sudo make install
```

## Configuration
The configuration file is /etc/yasu.conf.
Each line in this file must have the following syntax: `user action [options]` action can be `allow` or `deny`, and options can be `nopasswd`, `preserve`, or `persist`.

Here is an example config file:
```
#this is a comment
root allow nopasswd
foo deny
bar allow preserve
baz allow preserve persist=50
```

### Configuration options

`allow`: allow user, needs a password by default.
`deny`: don't allow the user: print a message without asking for a password.
`preserve`: preserve environment, by default only `TERM` is preserved.
`nopasswd`: requires the `allow` option, give permissions without asking for a password.
`persist`: requires the `allow` option, can't be used with `nopassword`, remember the user for 300seconds (by default), you can change this by adding an equal, and then the desired number of seconds (see the above example config file)

## Usage
```
yasu [-p] [-u user] -i
yasu [-p] [-u user] command arg1 arg2...
```

### Options
`-p`: preserve environment

`-i`: run an interactive shell

`-u user`: execute as user
