#define _GNU_SOURCE
#include <errno.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <shadow.h>
#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "yasu.h"
#include "bison.h"

#define MIN_PERSIST 5

extern FILE *yyin;
extern int ruleindex;

bool checkfilesafety(const char *path){
  struct stat filestat;
  if (stat(path,&filestat) != 0){
    return false;
  }
  if (filestat.st_uid != 0) { // Belongs to root
    return false;
  }
  mode_t mode = filestat.st_mode;
  if (mode & (S_IWGRP | S_IRGRP | S_IWOTH | S_IROTH)){ // not -rw------- permissions
    return false;
  }
  return true;
}
time_t gettimestamp(const char *username) {
  char *path;
  asprintf(&path,"/var/run/yasu/%s",username);
  if (checkfilesafety(path) == false) {
    free(path);
    return -1;
  }
  int fd = open(path,O_RDONLY);
  if (fd < 0) {
    free(path);
    close(fd);
    perror("open");
    return -1;
  }
  time_t timestamp;
  if (read(fd,&timestamp,sizeof(time_t)) != sizeof(time_t)){
    free(path);
    close(fd);
    return -1;
  }
  free(path);
  close(fd);
  return timestamp;
}

int settimestamp(const char *username){
  time_t timestamp = time(NULL);
  char *path;
  if (access("/var/run/yasu", F_OK) != 0){
    if (mkdir("/var/run/yasu",S_IWUSR | S_IRUSR) != 0) {
      perror("mkdir");
      return -1;
    }
  }
  asprintf(&path,"/var/run/yasu/%s",username);
  int fd = open(path, O_CREAT | O_RDWR, S_IWUSR | S_IRUSR);
  if (write(fd, &timestamp, sizeof(time_t)) != sizeof(time_t)){
    return -1;
  }
  close(fd);
  return 0;
}

static rule *getuserconfig(const char *username) {
  if (rules == NULL){
    return NULL;
  }
  rule *r;
  for (int i=0;i<ruleindex;i++){
    r = rules[i];
    if (!strcmp(username,r->user)){
      return r;
    }
  }
  return NULL;
}

static bool checkpersist(const char *user, rule *userconfig){
  int delay;
  if (userconfig->persisttime <= 0){
    delay = 5*60; // defaults to 5min
  }else{
    delay = userconfig->persisttime;
  }
  time_t user_ts = gettimestamp(user);
  if (user_ts < 0) { // Error reading timestamp
    return false;
  }
  time_t diff = time(NULL) - user_ts;
  if (diff < delay){
    return true;
  }
  return false;
}

static bool checkpasswd(const char *user){
  // Use PAM to authenticate user
  pam_handle_t *pamh = NULL;
  struct pam_conv conv = {
    misc_conv,
    NULL
  };
  int pam_status = pam_start("check_user", user, &conv, &pamh);
  if (pam_status != PAM_SUCCESS){
    free(pamh);
    return false;
  }
  pam_status = pam_authenticate(pamh, 0);    // is user really user?
  if (pam_status != PAM_SUCCESS){
    free(pamh);
    return false;
  }
  pam_status = pam_acct_mgmt(pamh, 0);       // permitted access?
  // Close PAM
  if (pam_end(pamh,pam_status) != PAM_SUCCESS){
    free(pamh);
    fputs("failed to end PAM\n",stderr);
    return false;
  }
  return pam_status == PAM_SUCCESS;
}

static int check(const char *user){
  rule *user_config = getuserconfig(user);
  if (!user_config) {
    return USER_UNKNOWN;
  }
  if (user_config->options & USER_ALLOW) {
    if (user_config->options & USER_NOPASSWD) { // No password
      return user_config->options; // Allow
    }
    if (user_config->options & USER_PERSIST && checkpersist(user, user_config)){
      return user_config->options;
    }
    // Ask the password
    for(int count=0;count < MAX_PASSWD_TRY;count++){
      if (count != 0)
        fputs("Try Again\n\n",stderr);
      if (checkpasswd(user)){
        return user_config->options; // Allow
      }
    }
    return (user_config->options - USER_ALLOW) | USER_BADPASSWD; // Block
  }else{
    return user_config->options; // Block
  }
}

static void print_usage(const char *name) {
  fprintf(stderr, "usage: %s [-p] [-u user] -i\n", name);
  fprintf(stderr, "       %s [-p] [-u user] command [args...]\n", name);
  fputc('\n', stderr);
  fputs("-u execute command as user\n", stderr);
  fputs("-i run interactive shell\n", stderr);
  fputs("-p preserve environment\n", stderr);
}

static int parseconfig(){
  FILE *conf = fopen(CONF_PATH,"r");
  if (conf == NULL){
    return -1;
  }
  yyin = conf;
  yyparse();
  fclose(conf);
  yyin = stdin;
  return 0;
}

static int freerules(){
  if (!rules) {
    return 0;
  }
  for (int i=0;i<ruleindex;i++){
    free(rules[i]);
  }
  free(rules);
  rules = NULL;
}

int main(int argc, char **argv) {
  (void)argc; // Disable "unused parameter" warning
  char **command;
  bool iflag = false, pflag = false;
  char *term, *user = "root", *name = argv[0];
  char *shellargs[2];
  extern char **environ;
  struct passwd *pw;
  struct passwd *caller_user = getpwuid(getuid());
  // Search argv for options
  while (*(++argv) != NULL) {
    if (!strcmp("-i", *argv)) {
      iflag = true;
    } else if (!strcmp("-p", *argv) || !strcmp("--preserve", *argv)) {
      pflag = true;
    } else if (!strcmp("-h", *argv) || !strcmp("--help", *argv)) {
      print_usage(name);
      return 0;
    } else if (!strcmp("-u", *argv) || !strcmp("--user", *argv)) {
      user = *(++argv);
      if (user == NULL){
        fputs("missing user after -u option\n",stderr);
        return 1;
      }
    } else if ((*argv)[0] == '-'){
      // Unknown option
      print_usage(name);
      return 1;
    } else {
      // End of options
      break;
    }
  }
  
  if (!iflag && *argv == NULL){
    fputs("missing command or -i option\n",stderr);
    return 1;
  }

  pw = getpwnam(user);

  if (!pw) {
    if (errno) {
      perror("getpwnam");
    } else {
      fprintf(stderr, "getpwnam %s: no such user\n", user);
    }
    return 1;
  }
  if (setuid(0) == -1) { // Become root: needed to check password
    perror("setuid");
    return 1;
  }
  if (initgroups(pw->pw_name, pw->pw_gid) == -1) {
    perror("initgroups");
    return 1;
  }
  if (setgid(pw->pw_gid) == -1) {
    perror("setgid");
    return 1;
  }
  if (parseconfig() < 0){
    fputs("Can't parse the configuration file: "CONF_PATH"\n", stderr);
    return 1;
  }
  int user_verif = check(caller_user->pw_name);
  if (user_verif & USER_ALLOW){ // Auth success
    if (user_verif & USER_PERSIST) {
      settimestamp(caller_user->pw_name);
    }
    if (setuid(pw->pw_uid) == -1){
      perror("setuid");
    } else {
      if (iflag){
        if (chdir(pw->pw_dir) == -1){
          perror("chdir");
        } else {
          // Start default shell if interactive
          shellargs[0] = getenv("SHELL");
          shellargs[1] = NULL;
          command = shellargs;
        }
      }else{
        command = argv;
      }
      if (!(pflag || user_verif & USER_PRESERVE)) {
        // Don't preserve environment
        term = getenv("TERM");
        clearenv();
        if (term) {
          setenv("TERM", term, 0);
        }
      }
      setenv("SHELL", pw->pw_shell[0] ? pw->pw_shell : "/bin/sh", 1);
      setenv("LOGNAME", pw->pw_name, 1);
      setenv("USER", pw->pw_name, 1);
      setenv("HOME", pw->pw_dir, 1);
      //setenv("PATH", "/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin", 1);
      freerules();
      execvp(command[0],command);
      perror("execvp");
    }
  }else if (user_verif & USER_UNKNOWN){
    fprintf(stderr,"user `%s` is not in the configuration file.\n",caller_user->pw_name);
  }else if (user_verif & USER_BADPASSWD){
    fputs("bad password\n",stderr);
  }else{
    fprintf(stderr,"user `%s` is not trusted.\n",caller_user->pw_name);
  }
  freerules();
  return 1;
}

