%{
#include "bison.h"
#include "yasu.h"
%}

%option noyywrap

whitespace [ \t\n]
comment #.*$
allow allow
deny deny
nopasswd nopasswd
preserve preserve
persist persist
numeric [0-9]+
user ^([._-]|[a-z])+
eq \=
%%
{whitespace} {} //pass
{comment} {} //pass
{user} {yylval.str = strdup(yytext); return TOKUSER;}
{allow} {return TOKALLOW;}
{deny} {return TOKDENY;}
{preserve} {yylval.option = USER_PRESERVE; return TOKOPTION;}
{persist} {yylval.option = USER_PERSIST; return TOKOPTION;}
{nopasswd} {yylval.option = USER_NOPASSWD; return TOKOPTION;}
{numeric} {yylval.number = atoi(yytext); return TOKNUMERIC;}
{eq} {return TOKEQ;}
