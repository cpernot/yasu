%{
#include <stdio.h>
#include <stdlib.h>
#include "yasu.h"
int yyerror(char *s);
size_t rulessize = 0;
int ruleindex = 0;
%}

%union {
  int option;
  int number;
  char *str;
  struct {
    int options;
    int persisttime;
  } options;
}
%token <option> TOKOPTION
%token <str> TOKUSER
%token <number> TOKNUMERIC
%token TOKEQ TOKALLOW TOKDENY
%type <options> persistopt options action
%%
grammar:
       %empty
       | grammar rule
       | error;
rule:
    TOKUSER action {
      rule *r = malloc(sizeof(rule));
      r->user = $1;
      r->options = $2.options;
      r->persisttime = $2.persisttime;
      if (ruleindex == rulessize){
        if (rulessize == 0){
          rulessize = 16;
        }else {
          rulessize *= 2;
        }
        if (!(rules = realloc(rules,rulessize*sizeof(rule *)))){
          yyerror("can't reallocate array");
          YYERROR;
        }
      }
      rules[ruleindex++] = r;
    }
action:
      TOKALLOW options {
        if ($2.options & USER_PERSIST && $2.options & USER_NOPASSWD) {
          yyerror("can't user nopasswd AND persist together");
          YYERROR;
        }
        $$.options = USER_ALLOW | $2.options;
        $$.persisttime = $2.persisttime;
      } | TOKDENY options {
        if ($2.options & (USER_PERSIST | USER_PRESERVE | USER_NOPASSWD)){
          yyerror("can't use deny AND nopasswd/persist/preserve together");
          YYERROR;
        }
        $$.options = $2.options;
        $$.persisttime = $2.persisttime;
      }
options:
       %empty {
          $$.options = 0;
          $$.persisttime = -1;
        } | options TOKOPTION {
          $$.options = $1.options | $2;
          $$.persisttime = $1.persisttime;
        } | options persistopt {
          $$.options = $1.options | $2.options;
          $$.persisttime = $2.persisttime;
        }
persistopt:
          TOKOPTION TOKEQ TOKNUMERIC {
            if (!($1 & USER_PERSIST)) {
              yyerror("can't use number argument for non-persist option");
              YYERROR;
            }
            $$.options = $1;
            $$.persisttime = $3;
          }
%%
int yyerror(char *s){
  fprintf(stderr,"yyerror: %s \n",s);
  return 0;
}
