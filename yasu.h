#ifndef YASU_H
#define YASU_H
#define USER_ALLOW (1)
#define USER_UNKNOWN (1<<1)
#define USER_NOPASSWD (1 << 2)
#define USER_BADPASSWD (1 << 3)
#define USER_PRESERVE (1 << 4)
#define USER_PERSIST (1 << 5)
#define CONF_PATH "/etc/yasu.conf"
#define MAX_PASSWD_TRY 2
typedef struct {
  int options;
  char *user;
  int persisttime;
} rule;
rule **rules;
#endif
